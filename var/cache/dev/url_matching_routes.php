<?php

/**
 * This file has been auto-generated
 * by the Symfony Routing Component.
 */

return [
    false, // $matchHost
    [ // $staticRoutes
        '/_profiler' => [[['_route' => '_profiler_home', '_controller' => 'web_profiler.controller.profiler::homeAction'], null, null, null, true, false, null]],
        '/_profiler/search' => [[['_route' => '_profiler_search', '_controller' => 'web_profiler.controller.profiler::searchAction'], null, null, null, false, false, null]],
        '/_profiler/search_bar' => [[['_route' => '_profiler_search_bar', '_controller' => 'web_profiler.controller.profiler::searchBarAction'], null, null, null, false, false, null]],
        '/_profiler/phpinfo' => [[['_route' => '_profiler_phpinfo', '_controller' => 'web_profiler.controller.profiler::phpinfoAction'], null, null, null, false, false, null]],
        '/_profiler/open' => [[['_route' => '_profiler_open_file', '_controller' => 'web_profiler.controller.profiler::openAction'], null, null, null, false, false, null]],
        '/admin' => [[['_route' => 'admin_home', '_controller' => 'App\\Controller\\AdminController::home'], null, null, null, true, false, null]],
        '/admin/add' => [[['_route' => 'admin_add', '_controller' => 'App\\Controller\\AdminController::form'], null, null, null, false, false, null]],
        '/admin/produits' => [[['_route' => 'admin_produits', '_controller' => 'App\\Controller\\AdminController::produits'], null, null, null, false, false, null]],
        '/admin/users' => [[['_route' => 'admin_users', '_controller' => 'App\\Controller\\AdminController::voirusers'], null, null, null, false, false, null]],
        '/admin/commandes' => [[['_route' => 'admin_commandes', '_controller' => 'App\\Controller\\CommandesController::commandes'], null, null, null, false, false, null]],
        '/connexion' => [[['_route' => 'security_connexion', '_controller' => 'App\\Controller\\SecurityController::connexion'], null, null, null, false, false, null]],
        '/inscription' => [[['_route' => 'security_inscription', '_controller' => 'App\\Controller\\SecurityController::registration'], null, null, null, false, false, null]],
        '/changepass' => [[['_route' => 'security_forgottenpass', '_controller' => 'App\\Controller\\SecurityController::forgottenpass'], null, null, null, false, false, null]],
        '/deconnexion' => [[['_route' => 'security_deconnexion', '_controller' => 'App\\Controller\\SecurityController::logout'], null, null, null, false, false, null]],
        '/security/compteuser' => [[['_route' => 'security_compteuser', '_controller' => 'App\\Controller\\SecurityController::user'], null, null, null, false, false, null]],
        '/panier' => [[['_route' => 'shop_panier', '_controller' => 'App\\Controller\\ShopController::index'], null, null, null, false, false, null]],
        '/' => [[['_route' => 'home', '_controller' => 'App\\Controller\\SiteController::home'], null, null, null, false, false, null]],
        '/about' => [[['_route' => 'about', '_controller' => 'App\\Controller\\SiteController::about'], null, null, null, false, false, null]],
        '/products' => [[['_route' => 'products', '_controller' => 'App\\Controller\\SiteController::prod'], null, null, null, false, false, null]],
        '/location' => [[['_route' => 'location', '_controller' => 'App\\Controller\\SiteController::loc'], null, null, null, false, false, null]],
        '/shop' => [[['_route' => 'shop', '_controller' => 'App\\Controller\\SiteController::shop'], null, null, null, false, false, null]],
        '/contact' => [[['_route' => 'contact', '_controller' => 'App\\Controller\\SiteController::contact'], null, null, null, false, false, null]],
    ],
    [ // $regexpList
        0 => '{^(?'
                .'|/_(?'
                    .'|error/(\\d+)(?:\\.([^/]++))?(*:38)'
                    .'|wdt/([^/]++)(*:57)'
                    .'|profiler/([^/]++)(?'
                        .'|/(?'
                            .'|search/results(*:102)'
                            .'|router(*:116)'
                            .'|exception(?'
                                .'|(*:136)'
                                .'|\\.css(*:149)'
                            .')'
                        .')'
                        .'|(*:159)'
                    .')'
                .')'
                .'|/a(?'
                    .'|dmin/(?'
                        .'|produits(?'
                            .'|([^/]++)/edit(*:206)'
                            .'|/(?'
                                .'|v(?'
                                    .'|oiravis/([^/]++)(*:238)'
                                    .'|alideavis/([^/]++)(*:264)'
                                .')'
                                .'|deleteavis/([^/]++)(*:292)'
                            .')'
                        .')'
                        .'|users/(?'
                            .'|([^/]++)(*:319)'
                            .'|deleteuser/([^/]++)(*:346)'
                        .')'
                        .'|commandedetail/([^/]++)(*:378)'
                        .'|setstatut/([^/]++)(*:404)'
                    .')'
                    .'|ctivation/([^/]++)(*:431)'
                .')'
                .'|/resetpass/([^/]++)(*:459)'
                .'|/s(?'
                    .'|ecurity/(?'
                        .'|compteuser([^/]++)/infouser(*:510)'
                        .'|detailcommande/([^/]++)(*:541)'
                    .')'
                    .'|hop/(?'
                        .'|([^/]++)(*:565)'
                        .'|avis/([^/]++)(*:586)'
                        .'|voiravis/([^/]++)(*:611)'
                    .')'
                .')'
                .'|/panier/(?'
                    .'|add/([^/]++)(*:644)'
                    .'|remove/([^/]++)(*:667)'
                .')'
            .')/?$}sDu',
    ],
    [ // $dynamicRoutes
        38 => [[['_route' => '_preview_error', '_controller' => 'error_controller::preview', '_format' => 'html'], ['code', '_format'], null, null, false, true, null]],
        57 => [[['_route' => '_wdt', '_controller' => 'web_profiler.controller.profiler::toolbarAction'], ['token'], null, null, false, true, null]],
        102 => [[['_route' => '_profiler_search_results', '_controller' => 'web_profiler.controller.profiler::searchResultsAction'], ['token'], null, null, false, false, null]],
        116 => [[['_route' => '_profiler_router', '_controller' => 'web_profiler.controller.router::panelAction'], ['token'], null, null, false, false, null]],
        136 => [[['_route' => '_profiler_exception', '_controller' => 'web_profiler.controller.exception_panel::body'], ['token'], null, null, false, false, null]],
        149 => [[['_route' => '_profiler_exception_css', '_controller' => 'web_profiler.controller.exception_panel::stylesheet'], ['token'], null, null, false, false, null]],
        159 => [[['_route' => '_profiler', '_controller' => 'web_profiler.controller.profiler::panelAction'], ['token'], null, null, false, true, null]],
        206 => [[['_route' => 'admin_edit', '_controller' => 'App\\Controller\\AdminController::form'], ['id'], null, null, false, false, null]],
        238 => [[['_route' => 'admin_voiravis', '_controller' => 'App\\Controller\\AdminController::voiravis'], ['id'], null, null, false, true, null]],
        264 => [[['_route' => 'admin_valideavis', '_controller' => 'App\\Controller\\AdminController::valideavis'], ['id'], null, null, false, true, null]],
        292 => [[['_route' => 'admin_deleteavis', '_controller' => 'App\\Controller\\AdminController::deleteavis'], ['id'], null, null, false, true, null]],
        319 => [[['_route' => 'admin_detailuser', '_controller' => 'App\\Controller\\AdminController::detailuser'], ['id'], null, null, false, true, null]],
        346 => [[['_route' => 'admin_deleteuser', '_controller' => 'App\\Controller\\AdminController::deleteuser'], ['id'], null, null, false, true, null]],
        378 => [[['_route' => 'admin_commandedetail', '_controller' => 'App\\Controller\\CommandesController::detail'], ['id'], null, null, false, true, null]],
        404 => [[['_route' => 'admin_setstatut', '_controller' => 'App\\Controller\\CommandesController::setstatut'], ['id'], null, null, false, true, null]],
        431 => [[['_route' => 'security_activation', '_controller' => 'App\\Controller\\SecurityController::activation'], ['token'], null, null, false, true, null]],
        459 => [[['_route' => 'security_resetpass', '_controller' => 'App\\Controller\\SecurityController::resetpass'], ['token'], null, null, false, true, null]],
        510 => [[['_route' => 'security_infouser', '_controller' => 'App\\Controller\\SecurityController::infouser'], ['id'], null, null, false, false, null]],
        541 => [[['_route' => 'detail_commande', '_controller' => 'App\\Controller\\SecurityController::detailCommande'], ['id'], null, null, false, true, null]],
        565 => [[['_route' => 'shop_show', '_controller' => 'App\\Controller\\SiteController::show'], ['id'], null, null, false, true, null]],
        586 => [[['_route' => 'shop_avis', '_controller' => 'App\\Controller\\SiteController::avis'], ['id'], null, null, false, true, null]],
        611 => [[['_route' => 'shop_voiravis', '_controller' => 'App\\Controller\\SiteController::voiravis'], ['id'], null, null, false, true, null]],
        644 => [[['_route' => 'shop_add', '_controller' => 'App\\Controller\\ShopController::add'], ['id'], null, null, false, true, null]],
        667 => [
            [['_route' => 'shop_remove', '_controller' => 'App\\Controller\\ShopController::remove'], ['id'], null, null, false, true, null],
            [null, null, null, null, false, false, 0],
        ],
    ],
    null, // $checkCondition
];
