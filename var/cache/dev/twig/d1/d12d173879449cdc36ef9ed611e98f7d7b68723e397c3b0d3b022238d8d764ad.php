<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/home.html.twig */
class __TwigTemplate_3dfc9d656e026f22f5f0931e17ef5b273dd80a137a13e23bdb7cfe08a6a97495 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/home.html.twig"));

        $this->parent = $this->loadTemplate("admin.html.twig", "admin/home.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Tableau de bord | ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "<div class=\"container py-5\">
    <h1 class=\"text-center\">Tableau de bord</h1>

<div class=\"row justify-content-center\">

            <div class=\"col-lg-4 col-md-12\">

            <h2 class=\"text-center\">Nouveaux avis</h2>

            ";
        // line 16
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["avis"]);
        foreach ($context['_seq'] as $context["_key"] => $context["avis"]) {
            // line 17
            echo "
            ";
            // line 18
            if ((twig_get_attribute($this->env, $this->source, $context["avis"], "valide", [], "any", false, false, false, 18) === false)) {
                // line 19
                echo "
            <div>
                <p>
                <span>Produit :</span><span>";
                // line 22
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["avis"], "produit", [], "any", false, false, false, 22), "nomproduit", [], "any", false, false, false, 22), "html", null, true);
                echo "</span>
                </p>
                <p>
                <span>User :</span><span>";
                // line 25
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["avis"], "user", [], "any", false, false, false, 25), "email", [], "any", false, false, false, 25), "html", null, true);
                echo "</span>
                </p>
                <p>
                <span>Contenu : </span><span>";
                // line 28
                echo twig_get_attribute($this->env, $this->source, $context["avis"], "contenu", [], "any", false, false, false, 28);
                echo "</span>
                </p>

                <a href=\"";
                // line 31
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_valideavis", ["id" => twig_get_attribute($this->env, $this->source, $context["avis"], "id", [], "any", false, false, false, 31)]), "html", null, true);
                echo "\" class=\"btn btn-primary\" onclick=\"return(confirm('Etes-vous sûr de vouloir valider cet avis ?'));\">Valider</a>
                <a href=\"";
                // line 32
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_deleteavis", ["id" => twig_get_attribute($this->env, $this->source, $context["avis"], "id", [], "any", false, false, false, 32)]), "html", null, true);
                echo "\" class=\"btn btn-danger\" onclick=\"return(confirm('Etes-vous sûr de vouloir supprimer cet avis ?'));\">Supprimer</a>
            </div>

            ";
            }
            // line 36
            echo "
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['avis'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 38
        echo "
            </div>
            <div class=\"col-lg-8 col-md-12\">

            <h2 class=\"text-center\">Nouvelles commandes</h2>

            ";
        // line 44
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["commande"]);
        foreach ($context['_seq'] as $context["_key"] => $context["commande"]) {
            // line 45
            echo "
            ";
            // line 46
            if ((twig_get_attribute($this->env, $this->source, $context["commande"], "statut", [], "any", false, false, false, 46) === "New")) {
                // line 47
                echo "
            <table class=\"table\">
            <tr class=\"table-success\">

            <td><span>Commande du : </span><span>";
                // line 51
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "date", [], "any", false, false, false, 51), "d/m/Y"), "html", null, true);
                echo "</span></td>

            <td><span>Prix total : </span><span>";
                // line 53
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "prixTotal", [], "any", false, false, false, 53), "html", null, true);
                echo "</span><span> € </span></td>

            <td><span>Statut : </span><span>";
                // line 55
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "statut", [], "any", false, false, false, 55), "html", null, true);
                echo "</span></td>

            <td><span>User : </span><span>";
                // line 57
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["commande"], "userId", [], "any", false, false, false, 57), "email", [], "any", false, false, false, 57), "html", null, true);
                echo "</span></td>

            <td><a class=\"btn btn-secondary\" href=\"";
                // line 59
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_commandedetail", ["id" => twig_get_attribute($this->env, $this->source, $context["commande"], "id", [], "any", false, false, false, 59)]), "html", null, true);
                echo "\">Détails</a></td>

            </tr>

            </table>

            ";
            }
            // line 66
            echo "
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['commande'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 68
        echo "
            </div>

        </div>

    </div>
</div>


";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  211 => 68,  204 => 66,  194 => 59,  189 => 57,  184 => 55,  179 => 53,  174 => 51,  168 => 47,  166 => 46,  163 => 45,  159 => 44,  151 => 38,  144 => 36,  137 => 32,  133 => 31,  127 => 28,  121 => 25,  115 => 22,  110 => 19,  108 => 18,  105 => 17,  101 => 16,  90 => 7,  80 => 6,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.html.twig' %}

{% block title %}Tableau de bord | {{ parent() }} {% endblock %}


{% block body %}
<div class=\"container py-5\">
    <h1 class=\"text-center\">Tableau de bord</h1>

<div class=\"row justify-content-center\">

            <div class=\"col-lg-4 col-md-12\">

            <h2 class=\"text-center\">Nouveaux avis</h2>

            {% for avis in avis %}

            {% if avis.valide is same as (false) %}

            <div>
                <p>
                <span>Produit :</span><span>{{ avis.produit.nomproduit }}</span>
                </p>
                <p>
                <span>User :</span><span>{{ avis.user.email }}</span>
                </p>
                <p>
                <span>Contenu : </span><span>{{ avis.contenu | raw }}</span>
                </p>

                <a href=\"{{ path('admin_valideavis', {'id': avis.id}) }}\" class=\"btn btn-primary\" onclick=\"return(confirm('Etes-vous sûr de vouloir valider cet avis ?'));\">Valider</a>
                <a href=\"{{ path('admin_deleteavis', {'id': avis.id}) }}\" class=\"btn btn-danger\" onclick=\"return(confirm('Etes-vous sûr de vouloir supprimer cet avis ?'));\">Supprimer</a>
            </div>

            {% endif %}

            {% endfor %}

            </div>
            <div class=\"col-lg-8 col-md-12\">

            <h2 class=\"text-center\">Nouvelles commandes</h2>

            {% for commande in commande %}

            {% if commande.statut is same as ('New') %}

            <table class=\"table\">
            <tr class=\"table-success\">

            <td><span>Commande du : </span><span>{{ commande.date | date('d/m/Y')  }}</span></td>

            <td><span>Prix total : </span><span>{{ commande.prixTotal }}</span><span> € </span></td>

            <td><span>Statut : </span><span>{{ commande.statut }}</span></td>

            <td><span>User : </span><span>{{ commande.userId.email }}</span></td>

            <td><a class=\"btn btn-secondary\" href=\"{{ path('admin_commandedetail', {'id': commande.id }) }}\">Détails</a></td>

            </tr>

            </table>

            {% endif %}

            {% endfor %}

            </div>

        </div>

    </div>
</div>


{% endblock %}", "admin/home.html.twig", "C:\\Users\\julij\\Desktop\\projettp\\templates\\admin\\home.html.twig");
    }
}
