<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* admin/voiravis.html.twig */
class __TwigTemplate_de640a8e454ffc6586a508f6cc7d7a80fd483d33890e50fd3ac549560b82a9f6 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "admin.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/voiravis.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "admin/voiravis.html.twig"));

        $this->parent = $this->loadTemplate("admin.html.twig", "admin/voiravis.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Admin | ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "<div class=\"container\">
<div class=\"row justify-content-center\">
<section class=\"produits\">

    <article class=\"py-2\">
        <h2>";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 12, $this->source); })()), "nomproduit", [], "any", false, false, false, 12), "html", null, true);
        echo "</h2>
            <img src=\"";
        // line 13
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 13, $this->source); })()), "image", [], "any", false, false, false, 13), "html", null, true);
        echo "\" alt=\"\">
        <div class=\"content\">
            <span>Description :</span>";
        // line 15
        echo twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 15, $this->source); })()), "description", [], "any", false, false, false, 15);
        echo "
            </br>
            <span>Composition :</span>";
        // line 17
        echo twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 17, $this->source); })()), "compo", [], "any", false, false, false, 17);
        echo "
            </br>
            <span>En stock : </span>";
        // line 19
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 19, $this->source); })()), "stock", [], "any", false, false, false, 19), "html", null, true);
        echo "
            </br>
            <span>Prix : </span>";
        // line 21
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 21, $this->source); })()), "prix", [], "any", false, false, false, 21), "html", null, true);
        echo " <span>€</span>
        </div>
    </article>

     <a href=\"";
        // line 25
        echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_produits");
        echo "\" class=\"btn btn-secondary\">Retour</a>

<h2 class=\"text-center\">";
        // line 27
        echo twig_escape_filter($this->env, twig_length_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 27, $this->source); })()), "avisClient", [], "any", false, false, false, 27)), "html", null, true);
        echo " avis</h2>

";
        // line 29
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["avis"]);
        foreach ($context['_seq'] as $context["_key"] => $context["avis"]) {
            // line 30
            echo "
<div class=\"row py-4\">
        <article>
            <h3>";
            // line 33
            echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, $context["avis"], "user", [], "any", false, false, false, 33), "prenom", [], "any", false, false, false, 33), "html", null, true);
            echo "</h3>
                <div class=\"content\">
                <span>Posté le : ";
            // line 35
            echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["avis"], "date", [], "any", false, false, false, 35), "d/m/Y"), "html", null, true);
            echo "</span>
                </br>
                    ";
            // line 37
            echo twig_get_attribute($this->env, $this->source, $context["avis"], "contenu", [], "any", false, false, false, 37);
            echo "
                </div>
        </article>
    </div>

            ";
            // line 42
            if ((twig_get_attribute($this->env, $this->source, $context["avis"], "valide", [], "any", false, false, false, 42) === false)) {
                // line 43
                echo "
            <a href=\"";
                // line 44
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_valideavis", ["id" => twig_get_attribute($this->env, $this->source, $context["avis"], "id", [], "any", false, false, false, 44)]), "html", null, true);
                echo "\" class=\"btn btn-primary\" onclick=\"return(confirm('Etes-vous sûr de vouloir valider cet avis ?'));\">Valider</a>
            <a href=\"";
                // line 45
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_deleteavis", ["id" => twig_get_attribute($this->env, $this->source, $context["avis"], "id", [], "any", false, false, false, 45)]), "html", null, true);
                echo "\" class=\"btn btn-danger\" onclick=\"return(confirm('Etes-vous sûr de vouloir supprimer cet avis ?'));\">Supprimer</a>

            ";
            } else {
                // line 48
                echo "
            <a href=\"";
                // line 49
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("admin_deleteavis", ["id" => twig_get_attribute($this->env, $this->source, $context["avis"], "id", [], "any", false, false, false, 49)]), "html", null, true);
                echo "\" class=\"btn btn-danger\" onclick=\"return(confirm('Etes-vous sûr de vouloir supprimer cet avis ?'));\">Supprimer</a>

            ";
            }
            // line 52
            echo "
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['avis'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 54
        echo "
</section>
</div>
</div>
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "admin/voiravis.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  196 => 54,  189 => 52,  183 => 49,  180 => 48,  174 => 45,  170 => 44,  167 => 43,  165 => 42,  157 => 37,  152 => 35,  147 => 33,  142 => 30,  138 => 29,  133 => 27,  128 => 25,  121 => 21,  116 => 19,  111 => 17,  106 => 15,  101 => 13,  97 => 12,  90 => 7,  80 => 6,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'admin.html.twig' %}

{% block title %}Admin | {{ parent() }} {% endblock %}


{% block body %}
<div class=\"container\">
<div class=\"row justify-content-center\">
<section class=\"produits\">

    <article class=\"py-2\">
        <h2>{{ produit.nomproduit }}</h2>
            <img src=\"{{ produit.image }}\" alt=\"\">
        <div class=\"content\">
            <span>Description :</span>{{ produit.description | raw }}
            </br>
            <span>Composition :</span>{{ produit.compo | raw }}
            </br>
            <span>En stock : </span>{{ produit.stock }}
            </br>
            <span>Prix : </span>{{ produit.prix }} <span>€</span>
        </div>
    </article>

     <a href=\"{{ path('admin_produits') }}\" class=\"btn btn-secondary\">Retour</a>

<h2 class=\"text-center\">{{ produit.avisClient | length }} avis</h2>

{% for avis in avis %}

<div class=\"row py-4\">
        <article>
            <h3>{{ avis.user.prenom }}</h3>
                <div class=\"content\">
                <span>Posté le : {{ avis.date | date('d/m/Y')}}</span>
                </br>
                    {{ avis.contenu | raw }}
                </div>
        </article>
    </div>

            {% if (avis.valide is same as(false)) %}

            <a href=\"{{ path('admin_valideavis', {'id': avis.id}) }}\" class=\"btn btn-primary\" onclick=\"return(confirm('Etes-vous sûr de vouloir valider cet avis ?'));\">Valider</a>
            <a href=\"{{ path('admin_deleteavis', {'id': avis.id}) }}\" class=\"btn btn-danger\" onclick=\"return(confirm('Etes-vous sûr de vouloir supprimer cet avis ?'));\">Supprimer</a>

            {% else %}

            <a href=\"{{ path('admin_deleteavis', {'id': avis.id}) }}\" class=\"btn btn-danger\" onclick=\"return(confirm('Etes-vous sûr de vouloir supprimer cet avis ?'));\">Supprimer</a>

            {% endif %}

            {% endfor %}

</section>
</div>
</div>
{% endblock %}", "admin/voiravis.html.twig", "C:\\Users\\julij\\Desktop\\projettp\\templates\\admin\\voiravis.html.twig");
    }
}
