<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* site/prod.html.twig */
class __TwigTemplate_d20db295a9de35661e66bd3a305696a62aa80032602bf7cc2a857414b59e7543 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "site/prod.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "site/prod.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "site/prod.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Nos produits | ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
<div class=\"container about\">

    <div class=\"row cadre\">
        <div class=\"col-lg-6 col-md-12\">
            <h2 class=\"text-uppercase text-center\">
                <span>Nos produits</span></h2>
            <div class=\"paragraph\">
                <p class=\"para\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                 Aenean pharetra lacus quam, nec dapibus enim ultrices eget. Duis sollicitudin,
                  justo vitae convallis efficitur, odio metus porta nisl, vel finibus arcu nisl vitae erat.
                   Praesent efficitur orci vitae elit aliquam tempus. Donec interdum purus in leo luctus,
                 sit amet tincidunt massa tempor. Duis tellus tortor, ultricies sed congue id, viverra sit amet massa.</p>
                <p class=\"para\">Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut et velit luctus, mattis leo sed, pellentesque est.
                   Nulla nibh nibh, consectetur id libero vitae, elementum ultrices turpis.
                    Nam bibendum non nulla eu gravida. Maecenas sollicitudin, neque quis molestie molestie, quam turpis fringilla enim,
                     laoreet volutpat nunc turpis a sapien. Aliquam non tristique turpis. Nam vehicula lacus eu maximus malesuada.</p>
            </div>
        </div>
        <div class=\"col-lg-6 col-md-12\">
            <div class=\"image\">
                <img src=\"/img/pickles4.jpg\" alt=\"\" class=\"img-fluid\" width=500>
            </div>
        </div>
    </div>

    <div class=\"row cadre\">
        <div class=\"col-lg-6 col-md-12\">
            <div class=\"image\">
                <img src=\"/img/vege.jpg\" alt=\"\" class=\"img-fluid\" width=500>
            </div>
        </div>
        <div class=\"col-lg-6 col-md-12\">
            <h2 class=\"text-uppercase text-center\">
                <span>Nos valeurs</span></h2>
            <div class=\"paragraph\">
                <p class=\"para\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                 Aenean pharetra lacus quam, nec dapibus enim ultrices eget. Duis sollicitudin,
                  justo vitae convallis efficitur, odio metus porta nisl, vel finibus arcu nisl vitae erat.
                   Praesent efficitur orci vitae elit aliquam tempus. Donec interdum purus in leo luctus,
                 sit amet tincidunt massa tempor. Duis tellus tortor, ultricies sed congue id, viverra sit amet massa.</p>
                <p class=\"para\">Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut et velit luctus, mattis leo sed, pellentesque est.
                   Nulla nibh nibh, consectetur id libero vitae, elementum ultrices turpis.
                    Nam bibendum non nulla eu gravida. Maecenas sollicitudin, neque quis molestie molestie, quam turpis fringilla enim,
                     laoreet volutpat nunc turpis a sapien. Aliquam non tristique turpis. Nam vehicula lacus eu maximus malesuada.</p>
            </div>
        </div>
    </div>

</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "site/prod.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  90 => 7,  80 => 6,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Nos produits | {{ parent() }} {% endblock %}


{% block body %}

<div class=\"container about\">

    <div class=\"row cadre\">
        <div class=\"col-lg-6 col-md-12\">
            <h2 class=\"text-uppercase text-center\">
                <span>Nos produits</span></h2>
            <div class=\"paragraph\">
                <p class=\"para\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                 Aenean pharetra lacus quam, nec dapibus enim ultrices eget. Duis sollicitudin,
                  justo vitae convallis efficitur, odio metus porta nisl, vel finibus arcu nisl vitae erat.
                   Praesent efficitur orci vitae elit aliquam tempus. Donec interdum purus in leo luctus,
                 sit amet tincidunt massa tempor. Duis tellus tortor, ultricies sed congue id, viverra sit amet massa.</p>
                <p class=\"para\">Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut et velit luctus, mattis leo sed, pellentesque est.
                   Nulla nibh nibh, consectetur id libero vitae, elementum ultrices turpis.
                    Nam bibendum non nulla eu gravida. Maecenas sollicitudin, neque quis molestie molestie, quam turpis fringilla enim,
                     laoreet volutpat nunc turpis a sapien. Aliquam non tristique turpis. Nam vehicula lacus eu maximus malesuada.</p>
            </div>
        </div>
        <div class=\"col-lg-6 col-md-12\">
            <div class=\"image\">
                <img src=\"/img/pickles4.jpg\" alt=\"\" class=\"img-fluid\" width=500>
            </div>
        </div>
    </div>

    <div class=\"row cadre\">
        <div class=\"col-lg-6 col-md-12\">
            <div class=\"image\">
                <img src=\"/img/vege.jpg\" alt=\"\" class=\"img-fluid\" width=500>
            </div>
        </div>
        <div class=\"col-lg-6 col-md-12\">
            <h2 class=\"text-uppercase text-center\">
                <span>Nos valeurs</span></h2>
            <div class=\"paragraph\">
                <p class=\"para\">Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                 Aenean pharetra lacus quam, nec dapibus enim ultrices eget. Duis sollicitudin,
                  justo vitae convallis efficitur, odio metus porta nisl, vel finibus arcu nisl vitae erat.
                   Praesent efficitur orci vitae elit aliquam tempus. Donec interdum purus in leo luctus,
                 sit amet tincidunt massa tempor. Duis tellus tortor, ultricies sed congue id, viverra sit amet massa.</p>
                <p class=\"para\">Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut et velit luctus, mattis leo sed, pellentesque est.
                   Nulla nibh nibh, consectetur id libero vitae, elementum ultrices turpis.
                    Nam bibendum non nulla eu gravida. Maecenas sollicitudin, neque quis molestie molestie, quam turpis fringilla enim,
                     laoreet volutpat nunc turpis a sapien. Aliquam non tristique turpis. Nam vehicula lacus eu maximus malesuada.</p>
            </div>
        </div>
    </div>

</div>

{% endblock %}
", "site/prod.html.twig", "C:\\Users\\julij\\Desktop\\projettp\\templates\\site\\prod.html.twig");
    }
}
