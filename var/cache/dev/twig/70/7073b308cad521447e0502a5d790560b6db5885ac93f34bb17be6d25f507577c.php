<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/compteuser.html.twig */
class __TwigTemplate_986bb844309a74576a2497e0de71b497700a4bc49d599a2ec358d64f7dc8b632 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/compteuser.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/compteuser.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "security/compteuser.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Mon compte | ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
<h1 class=\"text-center\">Mon compte</h1>

    <div class=\"container py-3\">

        <div class=\"row justify-content-center\">

            <div class=\"col-lg-4 col-md-12\">

            <h2 class=\"text-center\">Mes informations</h2>

                <p>
                <span>Nom :</span><span>";
        // line 18
        echo twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 18, $this->source); })()), "nom", [], "any", false, false, false, 18);
        echo "</span>
                </p>
                <p>
                <span>Prénom :</span><span>";
        // line 21
        echo twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 21, $this->source); })()), "prenom", [], "any", false, false, false, 21);
        echo "</span>
                </p>
                <p>
                <span>Email : </span><span>";
        // line 24
        echo twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 24, $this->source); })()), "email", [], "any", false, false, false, 24);
        echo "</span>
                </p>
                <p>
                <span>Telephone : </span><span>";
        // line 27
        echo twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 27, $this->source); })()), "telephone", [], "any", false, false, false, 27);
        echo "</span>
                </p>
                <p>
                <span>Adresse : </span><span>";
        // line 30
        echo twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 30, $this->source); })()), "adresse", [], "any", false, false, false, 30);
        echo "</span>
                </p>
                <p>
                <span>Code postal : </span><span>";
        // line 33
        echo twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 33, $this->source); })()), "codepostal", [], "any", false, false, false, 33);
        echo "</span>
                </p>
                <p>
                <span>Commune : </span><span>";
        // line 36
        echo twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 36, $this->source); })()), "commune", [], "any", false, false, false, 36);
        echo "</span>
                </p>
                <p>
                <span>Pays : </span><span>";
        // line 39
        echo twig_get_attribute($this->env, $this->source, (isset($context["user"]) || array_key_exists("user", $context) ? $context["user"] : (function () { throw new RuntimeError('Variable "user" does not exist.', 39, $this->source); })()), "pays", [], "any", false, false, false, 39);
        echo "</span>
                </p>

                <a class=\"btn btn-danger\" href=\"";
        // line 42
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_infouser", ["id" => twig_get_attribute($this->env, $this->source, twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 42, $this->source); })()), "user", [], "any", false, false, false, 42), "id", [], "any", false, false, false, 42)]), "html", null, true);
        echo "\">Modifier mes informations</a>

            </div>
            <div class=\"col-lg-8 col-md-12\">

            <h2 class=\"text-center\">Mes commandes</h2>

            ";
        // line 49
        $context['_parent'] = $context;
        $context['_seq'] = twig_ensure_traversable($context["commande"]);
        foreach ($context['_seq'] as $context["_key"] => $context["commande"]) {
            // line 50
            echo "
            ";
            // line 51
            if ((twig_get_attribute($this->env, $this->source, $context["commande"], "statut", [], "any", false, false, false, 51) === "New")) {
                // line 52
                echo "
            <table class=\"table\">
            <tr class=\"table-success\">

            <td><span>Commande du : </span><span>";
                // line 56
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "date", [], "any", false, false, false, 56), "d/m/Y"), "html", null, true);
                echo "</span></td>

            <td><span>Prix total : </span><span>";
                // line 58
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "prixTotal", [], "any", false, false, false, 58), "html", null, true);
                echo "</span><span> € </span></td>

            <td><span>Statut : </span><span>";
                // line 60
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "statut", [], "any", false, false, false, 60), "html", null, true);
                echo "</span></td>

            <td><span>Retrait : </span><span>";
                // line 62
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "retrait", [], "any", false, false, false, 62), "html", null, true);
                echo "</span></td>

            <td><a class=\"btn btn-secondary\" href=\"";
                // line 64
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("detail_commande", ["id" => twig_get_attribute($this->env, $this->source, $context["commande"], "id", [], "any", false, false, false, 64)]), "html", null, true);
                echo "\">Détails</a></td>

            </tr>

            </table>

            ";
            } else {
                // line 71
                echo "
            <table class=\"table\">
            <tr>

            <td><span>Commande du : </span><span>";
                // line 75
                echo twig_escape_filter($this->env, twig_date_format_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "date", [], "any", false, false, false, 75), "d/m/Y"), "html", null, true);
                echo "</span></td>

            <td><span>Prix total : </span><span>";
                // line 77
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "prixTotal", [], "any", false, false, false, 77), "html", null, true);
                echo "</span><span> € </span></td>

            <td><span>Statut : </span><span>";
                // line 79
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "statut", [], "any", false, false, false, 79), "html", null, true);
                echo "</span></td>

            <td><span>Retrait : </span><span>";
                // line 81
                echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, $context["commande"], "retrait", [], "any", false, false, false, 81), "html", null, true);
                echo "</span></td>

            <td><a class=\"btn btn-secondary\" href=\"";
                // line 83
                echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("detail_commande", ["id" => twig_get_attribute($this->env, $this->source, $context["commande"], "id", [], "any", false, false, false, 83)]), "html", null, true);
                echo "\">Détails</a></td>

            </tr>

            </table>

            ";
            }
            // line 90
            echo "
            ";
        }
        $_parent = $context['_parent'];
        unset($context['_seq'], $context['_iterated'], $context['_key'], $context['commande'], $context['_parent'], $context['loop']);
        $context = array_intersect_key($context, $_parent) + $_parent;
        // line 92
        echo "
            </div>

        </div>

    </div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/compteuser.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  250 => 92,  243 => 90,  233 => 83,  228 => 81,  223 => 79,  218 => 77,  213 => 75,  207 => 71,  197 => 64,  192 => 62,  187 => 60,  182 => 58,  177 => 56,  171 => 52,  169 => 51,  166 => 50,  162 => 49,  152 => 42,  146 => 39,  140 => 36,  134 => 33,  128 => 30,  122 => 27,  116 => 24,  110 => 21,  104 => 18,  90 => 6,  80 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Mon compte | {{ parent() }} {% endblock %}

{% block body %}

<h1 class=\"text-center\">Mon compte</h1>

    <div class=\"container py-3\">

        <div class=\"row justify-content-center\">

            <div class=\"col-lg-4 col-md-12\">

            <h2 class=\"text-center\">Mes informations</h2>

                <p>
                <span>Nom :</span><span>{{ user.nom | raw }}</span>
                </p>
                <p>
                <span>Prénom :</span><span>{{ user.prenom | raw }}</span>
                </p>
                <p>
                <span>Email : </span><span>{{ user.email | raw }}</span>
                </p>
                <p>
                <span>Telephone : </span><span>{{ user.telephone | raw }}</span>
                </p>
                <p>
                <span>Adresse : </span><span>{{ user.adresse | raw }}</span>
                </p>
                <p>
                <span>Code postal : </span><span>{{ user.codepostal | raw }}</span>
                </p>
                <p>
                <span>Commune : </span><span>{{ user.commune | raw }}</span>
                </p>
                <p>
                <span>Pays : </span><span>{{ user.pays | raw }}</span>
                </p>

                <a class=\"btn btn-danger\" href=\"{{ path('security_infouser', {'id': app.user.id}) }}\">Modifier mes informations</a>

            </div>
            <div class=\"col-lg-8 col-md-12\">

            <h2 class=\"text-center\">Mes commandes</h2>

            {% for commande in commande %}

            {% if commande.statut is same as ('New') %}

            <table class=\"table\">
            <tr class=\"table-success\">

            <td><span>Commande du : </span><span>{{ commande.date | date('d/m/Y')  }}</span></td>

            <td><span>Prix total : </span><span>{{ commande.prixTotal }}</span><span> € </span></td>

            <td><span>Statut : </span><span>{{ commande.statut }}</span></td>

            <td><span>Retrait : </span><span>{{ commande.retrait }}</span></td>

            <td><a class=\"btn btn-secondary\" href=\"{{ path('detail_commande', {'id': commande.id }) }}\">Détails</a></td>

            </tr>

            </table>

            {% else %}

            <table class=\"table\">
            <tr>

            <td><span>Commande du : </span><span>{{ commande.date | date('d/m/Y')  }}</span></td>

            <td><span>Prix total : </span><span>{{ commande.prixTotal }}</span><span> € </span></td>

            <td><span>Statut : </span><span>{{ commande.statut }}</span></td>

            <td><span>Retrait : </span><span>{{ commande.retrait }}</span></td>

            <td><a class=\"btn btn-secondary\" href=\"{{ path('detail_commande', {'id': commande.id }) }}\">Détails</a></td>

            </tr>

            </table>

            {% endif %}

            {% endfor %}

            </div>

        </div>

    </div>

{% endblock %}", "security/compteuser.html.twig", "C:\\Users\\julij\\Desktop\\projettp\\templates\\security\\compteuser.html.twig");
    }
}
