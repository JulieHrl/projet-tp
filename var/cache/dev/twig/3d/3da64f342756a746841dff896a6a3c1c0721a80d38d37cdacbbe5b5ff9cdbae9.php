<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* site/home.html.twig */
class __TwigTemplate_005774e4aedaa0ca6bc870cd59f430bd68c171ab94a0c4d4a1d5a4bd374e90f5 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "site/home.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "site/home.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "site/home.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo " ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 5
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 6
        echo "
<header class=\"masthead\">
    <div>
        <div class=\"container\">
            <div class=\"brand row justify-content-center align-items-center\">
                <div class=\"logo col-lg-2\">
                    <img class=\"img-fluid\" src=\"";
        // line 12
        echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\AssetExtension']->getAssetUrl("/img/logo.png"), "html", null, true);
        echo "\" alt=\"logo\"/>
                </div>
                <div class=\"col-lg-6\">
                    <h1 class=\"text-center brand-title text-responsive\">L'apéro des potes</h1>
                </div>
            </div>
        </div>
    </div>
</header>

<section class=\"py-5\">
    <div class=\"container\">
        <h2>Bienvenue à l'apéro des potes!</h2>
        <h3>Le nouveau condiment bio, local et plein de pep's!<h3>
        <h4>Pickle your life !</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                 Aenean pharetra lacus quam, nec dapibus enim ultrices eget. Duis sollicitudin,
                  justo vitae convallis efficitur, odio metus porta nisl, vel finibus arcu nisl vitae erat.
                   Praesent efficitur orci vitae elit aliquam tempus. Donec interdum purus in leo luctus,
                 sit amet tincidunt massa tempor. Duis tellus tortor, ultricies sed congue id, viverra sit amet massa.</p>
        <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut et velit luctus, mattis leo sed, pellentesque est.
                   Nulla nibh nibh, consectetur id libero vitae, elementum ultrices turpis.
                    Nam bibendum non nulla eu gravida. Maecenas sollicitudin, neque quis molestie molestie, quam turpis fringilla enim,
                     laoreet volutpat nunc turpis a sapien. Aliquam non tristique turpis. Nam vehicula lacus eu maximus malesuada.</p>
    </div>
</section>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "site/home.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  98 => 12,  90 => 6,  80 => 5,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %} {{ parent() }} {% endblock %}

{% block body %}

<header class=\"masthead\">
    <div>
        <div class=\"container\">
            <div class=\"brand row justify-content-center align-items-center\">
                <div class=\"logo col-lg-2\">
                    <img class=\"img-fluid\" src=\"{{ asset('/img/logo.png') }}\" alt=\"logo\"/>
                </div>
                <div class=\"col-lg-6\">
                    <h1 class=\"text-center brand-title text-responsive\">L'apéro des potes</h1>
                </div>
            </div>
        </div>
    </div>
</header>

<section class=\"py-5\">
    <div class=\"container\">
        <h2>Bienvenue à l'apéro des potes!</h2>
        <h3>Le nouveau condiment bio, local et plein de pep's!<h3>
        <h4>Pickle your life !</h4>
        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit.
                 Aenean pharetra lacus quam, nec dapibus enim ultrices eget. Duis sollicitudin,
                  justo vitae convallis efficitur, odio metus porta nisl, vel finibus arcu nisl vitae erat.
                   Praesent efficitur orci vitae elit aliquam tempus. Donec interdum purus in leo luctus,
                 sit amet tincidunt massa tempor. Duis tellus tortor, ultricies sed congue id, viverra sit amet massa.</p>
        <p>Interdum et malesuada fames ac ante ipsum primis in faucibus. Ut et velit luctus, mattis leo sed, pellentesque est.
                   Nulla nibh nibh, consectetur id libero vitae, elementum ultrices turpis.
                    Nam bibendum non nulla eu gravida. Maecenas sollicitudin, neque quis molestie molestie, quam turpis fringilla enim,
                     laoreet volutpat nunc turpis a sapien. Aliquam non tristique turpis. Nam vehicula lacus eu maximus malesuada.</p>
    </div>
</section>

{% endblock %}", "site/home.html.twig", "C:\\Users\\julij\\Desktop\\projettp\\templates\\site\\home.html.twig");
    }
}
