<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* security/inscription.html.twig */
class __TwigTemplate_b5e6e623fb0b8769a02285a8e4a1aabd20c69c8316a9457a18a73da7c4715ccc extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/inscription.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "security/inscription.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "security/inscription.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Inscription | ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
<h1 class=\"text-center\">Inscription</h1>

<div class=\"container\">
    <div class=\"row justify-content-center\">
        <div class=\"col-lg-7\">

            ";
        // line 14
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 14, $this->source); })()), 'form_start');
        echo "

            ";
        // line 16
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 16, $this->source); })()), "nom", [], "any", false, false, false, 16), 'row', ["attr" => ["placeholder" => "Entrez votre nom"]]);
        echo "

            ";
        // line 18
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 18, $this->source); })()), "prenom", [], "any", false, false, false, 18), 'row', ["label" => "Prénom", "attr" => ["placeholder" => "Entrez votre prénom"]]);
        echo "

            ";
        // line 20
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 20, $this->source); })()), "email", [], "any", false, false, false, 20), 'row', ["attr" => ["placeholder" => "Entrez votre email"]]);
        echo "

            ";
        // line 22
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 22, $this->source); })()), "telephone", [], "any", false, false, false, 22), 'row', ["label" => "Téléphone", "attr" => ["placeholder" => "Entrez votre numéro de téléphone (sans espaces ni ponctuation; ex: 0102030405)"]]);
        echo "

            ";
        // line 24
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 24, $this->source); })()), "adresse", [], "any", false, false, false, 24), 'row', ["attr" => ["placeholder" => "Entrez votre adresse"]]);
        echo "

            ";
        // line 26
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 26, $this->source); })()), "codepostal", [], "any", false, false, false, 26), 'row', ["label" => "Code postal", "attr" => ["placeholder" => "Entrez votre code postal"]]);
        echo "

            ";
        // line 28
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 28, $this->source); })()), "commune", [], "any", false, false, false, 28), 'row', ["attr" => ["placeholder" => "Entrez votre commune"]]);
        echo "

            ";
        // line 30
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 30, $this->source); })()), "pays", [], "any", false, false, false, 30), 'row', ["attr" => ["placeholder" => "Entrez votre pays"]]);
        echo "

            ";
        // line 32
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 32, $this->source); })()), "password", [], "any", false, false, false, 32), 'row', ["label" => "Mot de passe", "attr" => ["placeholder" => "Entrez votre mot de passe"]]);
        echo "

            ";
        // line 34
        echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 34, $this->source); })()), "confirm_password", [], "any", false, false, false, 34), 'row', ["label" => "Confirmation du mot de passe", "attr" => ["placeholder" => "Confirmez le mot de passe"]]);
        echo "

            <button type=\"submit\" class=\"btn btn-success\">S'inscrire</button>

            ";
        // line 38
        echo         $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["formUser"]) || array_key_exists("formUser", $context) ? $context["formUser"] : (function () { throw new RuntimeError('Variable "formUser" does not exist.', 38, $this->source); })()), 'form_end');
        echo "




        </div>
    </div>
</div>

";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "security/inscription.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  156 => 38,  149 => 34,  144 => 32,  139 => 30,  134 => 28,  129 => 26,  124 => 24,  119 => 22,  114 => 20,  109 => 18,  104 => 16,  99 => 14,  90 => 7,  80 => 6,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Inscription | {{ parent() }} {% endblock %}


{% block body %}

<h1 class=\"text-center\">Inscription</h1>

<div class=\"container\">
    <div class=\"row justify-content-center\">
        <div class=\"col-lg-7\">

            {{ form_start(formUser) }}

            {{ form_row(formUser.nom, {'attr': {'placeholder': \"Entrez votre nom\"}}) }}

            {{ form_row(formUser.prenom, {'label': 'Prénom', 'attr': {'placeholder': \"Entrez votre prénom\"}}) }}

            {{ form_row(formUser.email, {'attr': {'placeholder': \"Entrez votre email\"}}) }}

            {{ form_row(formUser.telephone, {'label': 'Téléphone', 'attr': {'placeholder': \"Entrez votre numéro de téléphone (sans espaces ni ponctuation; ex: 0102030405)\"}}) }}

            {{ form_row(formUser.adresse, {'attr': {'placeholder': \"Entrez votre adresse\"}}) }}

            {{ form_row(formUser.codepostal, {'label': 'Code postal', 'attr': {'placeholder': \"Entrez votre code postal\"}}) }}

            {{ form_row(formUser.commune, {'attr': {'placeholder': \"Entrez votre commune\"}}) }}

            {{ form_row(formUser.pays, {'attr': {'placeholder': \"Entrez votre pays\"}}) }}

            {{ form_row(formUser.password, {'label': 'Mot de passe', 'attr': {'placeholder': \"Entrez votre mot de passe\"}}) }}

            {{ form_row(formUser.confirm_password, {'label': 'Confirmation du mot de passe', 'attr': {'placeholder': \"Confirmez le mot de passe\"}}) }}

            <button type=\"submit\" class=\"btn btn-success\">S'inscrire</button>

            {{ form_end(formUser) }}




        </div>
    </div>
</div>

{% endblock %}", "security/inscription.html.twig", "C:\\Users\\julij\\Desktop\\projettp\\templates\\security\\inscription.html.twig");
    }
}
