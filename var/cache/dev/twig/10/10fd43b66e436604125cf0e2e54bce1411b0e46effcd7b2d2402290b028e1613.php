<?php

use Twig\Environment;
use Twig\Error\LoaderError;
use Twig\Error\RuntimeError;
use Twig\Extension\SandboxExtension;
use Twig\Markup;
use Twig\Sandbox\SecurityError;
use Twig\Sandbox\SecurityNotAllowedTagError;
use Twig\Sandbox\SecurityNotAllowedFilterError;
use Twig\Sandbox\SecurityNotAllowedFunctionError;
use Twig\Source;
use Twig\Template;

/* site/avis.html.twig */
class __TwigTemplate_39212b0bda3ab69532bd7f11ce888cc24a8bb495504129f622e15aac4e439c30 extends Template
{
    private $source;
    private $macros = [];

    public function __construct(Environment $env)
    {
        parent::__construct($env);

        $this->source = $this->getSourceContext();

        $this->blocks = [
            'title' => [$this, 'block_title'],
            'body' => [$this, 'block_body'],
        ];
    }

    protected function doGetParent(array $context)
    {
        // line 1
        return "base.html.twig";
    }

    protected function doDisplay(array $context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "site/avis.html.twig"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "template", "site/avis.html.twig"));

        $this->parent = $this->loadTemplate("base.html.twig", "site/avis.html.twig", 1);
        $this->parent->display($context, array_merge($this->blocks, $blocks));
        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

    }

    // line 3
    public function block_title($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "title"));

        echo "Laisser un avis | ";
        $this->displayParentBlock("title", $context, $blocks);
        echo " ";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    // line 6
    public function block_body($context, array $blocks = [])
    {
        $macros = $this->macros;
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e = $this->extensions["Symfony\\Bundle\\WebProfilerBundle\\Twig\\WebProfilerExtension"];
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->enter($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02 = $this->extensions["Symfony\\Bridge\\Twig\\Extension\\ProfilerExtension"];
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->enter($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof = new \Twig\Profiler\Profile($this->getTemplateName(), "block", "body"));

        // line 7
        echo "
<h1 class=\"text-center\">Laisser un avis</h1>
<div class=\"container\">
<div class=\"row justify-content-center py-5\">
<article>
    <h2 class=\"text-center\">";
        // line 12
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 12, $this->source); })()), "nomproduit", [], "any", false, false, false, 12), "html", null, true);
        echo "</h2>

        <img class=\"center-block\" src=\"";
        // line 14
        echo twig_escape_filter($this->env, twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 14, $this->source); })()), "image", [], "any", false, false, false, 14), "html", null, true);
        echo "\" alt=\"\">
    <div class=\"content\">
        ";
        // line 16
        echo twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 16, $this->source); })()), "description", [], "any", false, false, false, 16);
        echo "
    </div>
</article>
</div>
</div>


";
        // line 23
        if (twig_get_attribute($this->env, $this->source, (isset($context["app"]) || array_key_exists("app", $context) ? $context["app"] : (function () { throw new RuntimeError('Variable "app" does not exist.', 23, $this->source); })()), "user", [], "any", false, false, false, 23)) {
            // line 24
            echo "<div class=\"container\">
<div class=\"row justify-content-center py-5\">

";
            // line 27
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["avisForm"]) || array_key_exists("avisForm", $context) ? $context["avisForm"] : (function () { throw new RuntimeError('Variable "avisForm" does not exist.', 27, $this->source); })()), 'form_start');
            echo "
";
            // line 28
            echo $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->searchAndRenderBlock(twig_get_attribute($this->env, $this->source, (isset($context["avisForm"]) || array_key_exists("avisForm", $context) ? $context["avisForm"] : (function () { throw new RuntimeError('Variable "avisForm" does not exist.', 28, $this->source); })()), "contenu", [], "any", false, false, false, 28), 'row', ["label" => "Laisser un avis", "attr" => ["style" => "width:350px; height:250px", "placeholder" => "Vous avez goûté? Dites-nous ce que vous en avez pensé!"]]);
            echo "
<button type=\"submit\" class=\"btn btn-success\" onclick=\"return(confirm('Etes-vous sûr de vouloir poster cet avis ?'));\">Laisser un avis</button>
<a href=\"";
            // line 30
            echo twig_escape_filter($this->env, $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("shop_show", ["id" => twig_get_attribute($this->env, $this->source, (isset($context["produit"]) || array_key_exists("produit", $context) ? $context["produit"] : (function () { throw new RuntimeError('Variable "produit" does not exist.', 30, $this->source); })()), "id", [], "any", false, false, false, 30)]), "html", null, true);
            echo "\" class=\"btn btn-secondary mx-2\">Retour</a>
";
            // line 31
            echo             $this->env->getRuntime('Symfony\Component\Form\FormRenderer')->renderBlock((isset($context["avisForm"]) || array_key_exists("avisForm", $context) ? $context["avisForm"] : (function () { throw new RuntimeError('Variable "avisForm" does not exist.', 31, $this->source); })()), 'form_end');
            echo "

</div>
</div>

";
        } else {
            // line 37
            echo "
<div class=\"container\">
    <div class=\"row justify-content-center py-3\">
        <p>Vous ne pouvez pas laisser d'avis si vous n'êtes pas connecté!</p>
    </div>
    <div class=\"row justify-content-center\">
        <a href=\"";
            // line 43
            echo $this->extensions['Symfony\Bridge\Twig\Extension\RoutingExtension']->getPath("security_connexion");
            echo "\" class=\"btn btn-primary\">Se connecter</a>
    </div>
</div>

";
        }
        // line 48
        echo "
";
        
        $__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02->leave($__internal_319393461309892924ff6e74d6d6e64287df64b63545b994e100d4ab223aed02_prof);

        
        $__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e->leave($__internal_085b0142806202599c7fe3b329164a92397d8978207a37e79d70b8c52599e33e_prof);

    }

    public function getTemplateName()
    {
        return "site/avis.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  162 => 48,  154 => 43,  146 => 37,  137 => 31,  133 => 30,  128 => 28,  124 => 27,  119 => 24,  117 => 23,  107 => 16,  102 => 14,  97 => 12,  90 => 7,  80 => 6,  59 => 3,  36 => 1,);
    }

    public function getSourceContext()
    {
        return new Source("{% extends 'base.html.twig' %}

{% block title %}Laisser un avis | {{ parent() }} {% endblock %}


{% block body %}

<h1 class=\"text-center\">Laisser un avis</h1>
<div class=\"container\">
<div class=\"row justify-content-center py-5\">
<article>
    <h2 class=\"text-center\">{{ produit.nomproduit }}</h2>

        <img class=\"center-block\" src=\"{{ produit.image }}\" alt=\"\">
    <div class=\"content\">
        {{ produit.description | raw }}
    </div>
</article>
</div>
</div>


{% if app.user %}
<div class=\"container\">
<div class=\"row justify-content-center py-5\">

{{ form_start(avisForm) }}
{{ form_row(avisForm.contenu, {'label': 'Laisser un avis', 'attr': {'style': 'width:350px; height:250px' , 'placeholder': \"Vous avez goûté? Dites-nous ce que vous en avez pensé!\"}}) }}
<button type=\"submit\" class=\"btn btn-success\" onclick=\"return(confirm('Etes-vous sûr de vouloir poster cet avis ?'));\">Laisser un avis</button>
<a href=\"{{ path('shop_show', {'id': produit.id}) }}\" class=\"btn btn-secondary mx-2\">Retour</a>
{{ form_end(avisForm) }}

</div>
</div>

{% else %}

<div class=\"container\">
    <div class=\"row justify-content-center py-3\">
        <p>Vous ne pouvez pas laisser d'avis si vous n'êtes pas connecté!</p>
    </div>
    <div class=\"row justify-content-center\">
        <a href=\"{{ path('security_connexion') }}\" class=\"btn btn-primary\">Se connecter</a>
    </div>
</div>

{% endif %}

{% endblock %}", "site/avis.html.twig", "C:\\Users\\julij\\Desktop\\projettp\\templates\\site\\avis.html.twig");
    }
}
