<?php

namespace Proxies\__CG__\App\Entity;

/**
 * DO NOT EDIT THIS FILE - IT WAS CREATED BY DOCTRINE'S PROXY GENERATOR
 */
class Produit extends \App\Entity\Produit implements \Doctrine\ORM\Proxy\Proxy
{
    /**
     * @var \Closure the callback responsible for loading properties in the proxy object. This callback is called with
     *      three parameters, being respectively the proxy object to be initialized, the method that triggered the
     *      initialization process and an array of ordered parameters that were passed to that method.
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setInitializer
     */
    public $__initializer__;

    /**
     * @var \Closure the callback responsible of loading properties that need to be copied in the cloned object
     *
     * @see \Doctrine\Common\Proxy\Proxy::__setCloner
     */
    public $__cloner__;

    /**
     * @var boolean flag indicating if this object was already initialized
     *
     * @see \Doctrine\Persistence\Proxy::__isInitialized
     */
    public $__isInitialized__ = false;

    /**
     * @var array<string, null> properties to be lazy loaded, indexed by property name
     */
    public static $lazyPropertiesNames = array (
);

    /**
     * @var array<string, mixed> default values of properties to be lazy loaded, with keys being the property names
     *
     * @see \Doctrine\Common\Proxy\Proxy::__getLazyProperties
     */
    public static $lazyPropertiesDefaults = array (
);



    public function __construct(?\Closure $initializer = null, ?\Closure $cloner = null)
    {

        $this->__initializer__ = $initializer;
        $this->__cloner__      = $cloner;
    }







    /**
     * 
     * @return array
     */
    public function __sleep()
    {
        if ($this->__isInitialized__) {
            return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'id', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'nomproduit', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'stock', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'conditionnement', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'prix', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'description', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'compo', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'image', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'avisClient'];
        }

        return ['__isInitialized__', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'id', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'nomproduit', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'stock', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'conditionnement', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'prix', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'description', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'compo', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'image', '' . "\0" . 'App\\Entity\\Produit' . "\0" . 'avisClient'];
    }

    /**
     * 
     */
    public function __wakeup()
    {
        if ( ! $this->__isInitialized__) {
            $this->__initializer__ = function (Produit $proxy) {
                $proxy->__setInitializer(null);
                $proxy->__setCloner(null);

                $existingProperties = get_object_vars($proxy);

                foreach ($proxy::$lazyPropertiesDefaults as $property => $defaultValue) {
                    if ( ! array_key_exists($property, $existingProperties)) {
                        $proxy->$property = $defaultValue;
                    }
                }
            };

        }
    }

    /**
     * 
     */
    public function __clone()
    {
        $this->__cloner__ && $this->__cloner__->__invoke($this, '__clone', []);
    }

    /**
     * Forces initialization of the proxy
     */
    public function __load()
    {
        $this->__initializer__ && $this->__initializer__->__invoke($this, '__load', []);
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __isInitialized()
    {
        return $this->__isInitialized__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitialized($initialized)
    {
        $this->__isInitialized__ = $initialized;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setInitializer(\Closure $initializer = null)
    {
        $this->__initializer__ = $initializer;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __getInitializer()
    {
        return $this->__initializer__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     */
    public function __setCloner(\Closure $cloner = null)
    {
        $this->__cloner__ = $cloner;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific cloning logic
     */
    public function __getCloner()
    {
        return $this->__cloner__;
    }

    /**
     * {@inheritDoc}
     * @internal generated method: use only when explicitly handling proxy specific loading logic
     * @deprecated no longer in use - generated code now relies on internal components rather than generated public API
     * @static
     */
    public function __getLazyProperties()
    {
        return self::$lazyPropertiesDefaults;
    }

    
    /**
     * {@inheritDoc}
     */
    public function getId(): ?int
    {
        if ($this->__isInitialized__ === false) {
            return (int)  parent::getId();
        }


        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getId', []);

        return parent::getId();
    }

    /**
     * {@inheritDoc}
     */
    public function getNomproduit(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getNomproduit', []);

        return parent::getNomproduit();
    }

    /**
     * {@inheritDoc}
     */
    public function setNomproduit(string $nomproduit): \App\Entity\Produit
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setNomproduit', [$nomproduit]);

        return parent::setNomproduit($nomproduit);
    }

    /**
     * {@inheritDoc}
     */
    public function getStock(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getStock', []);

        return parent::getStock();
    }

    /**
     * {@inheritDoc}
     */
    public function setStock(?int $stock): \App\Entity\Produit
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setStock', [$stock]);

        return parent::setStock($stock);
    }

    /**
     * {@inheritDoc}
     */
    public function getConditionnement(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getConditionnement', []);

        return parent::getConditionnement();
    }

    /**
     * {@inheritDoc}
     */
    public function setConditionnement(int $conditionnement): \App\Entity\Produit
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setConditionnement', [$conditionnement]);

        return parent::setConditionnement($conditionnement);
    }

    /**
     * {@inheritDoc}
     */
    public function getPrix(): ?int
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getPrix', []);

        return parent::getPrix();
    }

    /**
     * {@inheritDoc}
     */
    public function setPrix(int $prix): \App\Entity\Produit
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setPrix', [$prix]);

        return parent::setPrix($prix);
    }

    /**
     * {@inheritDoc}
     */
    public function getDescription(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getDescription', []);

        return parent::getDescription();
    }

    /**
     * {@inheritDoc}
     */
    public function setDescription(string $description): \App\Entity\Produit
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setDescription', [$description]);

        return parent::setDescription($description);
    }

    /**
     * {@inheritDoc}
     */
    public function getCompo(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getCompo', []);

        return parent::getCompo();
    }

    /**
     * {@inheritDoc}
     */
    public function setCompo(string $compo): \App\Entity\Produit
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setCompo', [$compo]);

        return parent::setCompo($compo);
    }

    /**
     * {@inheritDoc}
     */
    public function getImage(): ?string
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getImage', []);

        return parent::getImage();
    }

    /**
     * {@inheritDoc}
     */
    public function setImage(string $image): \App\Entity\Produit
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'setImage', [$image]);

        return parent::setImage($image);
    }

    /**
     * {@inheritDoc}
     */
    public function getAvisClient(): \Doctrine\Common\Collections\Collection
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'getAvisClient', []);

        return parent::getAvisClient();
    }

    /**
     * {@inheritDoc}
     */
    public function addAvisClient(\App\Entity\Avis $avisClient): \App\Entity\Produit
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'addAvisClient', [$avisClient]);

        return parent::addAvisClient($avisClient);
    }

    /**
     * {@inheritDoc}
     */
    public function removeAvisClient(\App\Entity\Avis $avisClient): \App\Entity\Produit
    {

        $this->__initializer__ && $this->__initializer__->__invoke($this, 'removeAvisClient', [$avisClient]);

        return parent::removeAvisClient($avisClient);
    }

}
