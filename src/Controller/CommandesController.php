<?php

namespace App\Controller;

use App\Entity\Commande;
use App\Repository\CommandeRepository;
use App\Repository\CommandeArticlesRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CommandesController extends AbstractController
{
    /**
     * @Route("admin/commandes", name="admin_commandes")
     */
    public function commandes(CommandeRepository $comRepo)
    {
        $commande = $comRepo->findAll();

        return $this->render('admin/commandes.html.twig', [
            'commande' => $commande
        ]);
    }

    /**
     * @Route("admin/commandedetail/{id}", name="admin_commandedetail")
     */
    public function detail($id, CommandeRepository $comRepo, CommandeArticlesRepository $artRepo)
    {
        $commande = $comRepo->find($id);
        $prodCommande = $artRepo->findAll();

        return $this->render('admin/commandedetail.html.twig', [
            'commande' => $commande,
            'produits' => $prodCommande
        ]);
    }

    /**
     * @Route("admin/setstatut/{id}", name="admin_setstatut")
     */
    public function setstatut($id, Commande $commande, CommandeRepository $comRepo, EntityManagerInterface $manager)
    {
        $commande = $comRepo->find($id);

        $statutCommande = $commande->getStatut();

        if ($statutCommande == 'New') {
            $commande->setStatut('En cours');
        } else if ($statutCommande == 'En cours') {
            $commande->setStatut('Prête');
        } else if ($statutCommande == 'Prête') {
            $commande->setStatut('Terminée');
        }

        $manager->persist($commande);
        $manager->flush();

        $this->addFlash('success', 'Le statut de la commande a bien été modifié');

        return $this->redirectToRoute('admin_commandes');
    }
}
