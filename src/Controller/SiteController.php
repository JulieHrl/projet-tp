<?php

namespace App\Controller;

use App\Entity\Avis;
use App\Form\AvisType;
use \App\Entity\Produit;
use App\Form\ContactType;
use App\Repository\AvisRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;


class SiteController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function home()
    {
        return $this->render('site/home.html.twig');
    }

    /**
     * @Route("/about", name="about")
     */
    public function about()
    {
        return $this->render('site/about.html.twig', [
            'controller_name' => 'SiteController',
        ]);
    }

    /**
     * @Route("/products", name="products")
     */
    public function prod()
    {
        return $this->render('site/prod.html.twig');
    }

    /**
     * @Route("/location", name="location")
     */
    public function loc()
    {
        return $this->render('site/loc.html.twig');
    }

    /**
     * @Route("/shop", name="shop")
     */
    public function shop(ProduitRepository $repo)
    {

        $produits = $repo->findAll();

        return $this->render('site/shop.html.twig', [
            'produits' => $produits
        ]);
    }

    /**
     * @Route("/shop/{id}", name="shop_show")
     */
    public function show(Produit $produit)
    {

        return $this->render('site/show.html.twig', [
            'produit' => $produit
        ]);
    }

    /**
     * @Route("/shop/avis/{id}", name="shop_avis")
     */
    public function avis(Produit $produit, Request $request, EntityManagerInterface $manager)
    {
        $avis = new Avis();

        $user = $this->getUser();

        $avis->setdate(new \DateTime())
            ->setvalide(false)
            ->setUser($user)
            ->setProduit($produit);


        $form = $this->createForm(AvisType::class, $avis);

        $form->handleRequest($request);

        if ($form->isSubmitted()) {

            $manager->persist($avis);
            $manager->flush();

            $this->addFlash('success', 'Merci! Votre avis est en attente de validation');

            return $this->redirectToRoute('shop_show', ['id' => $produit->getId()]);
        }

        return $this->render('site/avis.html.twig', [
            'produit' => $produit,
            'avisForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/shop/voiravis/{id}", name="shop_voiravis")
     */
    public function voiravis(Produit $produit, AvisRepository $repo)
    {

        $avis = $produit->getAvisClient();

        return $this->render('site/voiravis.html.twig', [
            'produit' => $produit,
            'avis' => $avis
        ]);
    }

    /**
     * @Route("/contact", name="contact")
     */
    public function contact(Request $request, MailerInterface $mailer)
    {

        $form = $this->createForm(ContactType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $contact = $form->getData();

            $message = (new TemplatedEmail())
                ->from($contact['email'])
                ->to('devjulie34@gmail.com')
                ->subject('Nouveau contact')
                ->htmlTemplate('emails/mailcontact.html.twig')
                ->context(['contact' => $contact]);

            $mailer->send($message);

            $this->addFlash('success', 'Le message a bien été envoyé');

            return $this->redirectToRoute('home');
        }

        return $this->render('site/contact.html.twig', [
            'contactForm' => $form->createView()
        ]);
    }
}
