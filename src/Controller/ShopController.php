<?php

namespace App\Controller;

use App\Entity\Commande;
use App\Entity\CommandeArticles;
use App\Form\RetraitType;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Session\SessionInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class ShopController extends AbstractController
{
    /**
     * @Route("/panier", name="shop_panier")
     * @Security("is_granted('ROLE_USER')")
     */
    public function index(SessionInterface $session, ProduitRepository $repo, Request $request, EntityManagerInterface $manager)
    {
        $panier = $session->get('panier', []);

        $panierWithData = [];

        foreach ($panier as $id => $quantity) {
            $panierWithData[] = [
                'produit' => $repo->find($id),
                'quantity' => $quantity
            ];
        }

        $total = 0;

        foreach ($panierWithData as $item) {
            $totalItem = $item['produit']->getPrix() * $item['quantity'];
            $total += $totalItem;
        }

        // On crée le form qui donne les options de retrait

        $commande = new Commande();

        $formRetrait = $this->createForm(RetraitType::class, $commande);

        $formRetrait->handleRequest($request);

        if ($formRetrait->isSubmitted() && $formRetrait->isValid()) {

            // On définit la commande

            foreach ($panierWithData as $item) {
                $commande->setPrixTotal($total);
                $commandeArticles = new CommandeArticles();
                $commandeArticles->setCommande($commande);
                $commandeArticles->setArticle($item['produit']);
                $commandeArticles->setQuantite($item['quantity']);
                $manager->persist($commandeArticles);
            }

            $user = $this->getUser();

            $commande->setdate(new \DateTime())
                ->setUserId($user)
                ->setStatut('New');

            $manager->persist($commande);
            $manager->flush();


            $panier = $session->set('panier', []);

            $this->addFlash('Success', 'Votre commande a bien été enregistrée!');

            return $this->redirectToRoute('security_compteuser');
        }
        return $this->render('shop/panier.html.twig', [
            'items' => $panierWithData,
            'total' => $total,
            'formRetrait' => $formRetrait->createView()
        ]);
    }

    /**
     * @Route("/panier/add/{id}", name="shop_add")
     * @Security("is_granted('ROLE_USER')")
     */
    public function add($id, SessionInterface $session)
    {

        // On instancie la session et le panier, qui sera un array vide par défaut

        $panier = $session->get('panier', []);

        // On définit les modalités d'ajout de produits au panier

        if (!empty($panier[$id])) {
            $panier[$id]++;
        } else {
            $panier[$id] = 1;
        }

        $session->set('panier', $panier);

        $this->addFlash('success', 'Le produit a bien été ajouté au panier!');

        return $this->redirectToRoute('shop');
    }

    /**
     * @Route("/panier/remove/{id}", name="shop_remove")
     * @Security("is_granted('ROLE_USER')")
     */
    public function remove($id, SessionInterface $session)
    {
        $panier = $session->get('panier', []);

        if (!empty($panier[$id])) {
            $panier[$id]--;
        }

        $session->set('panier', $panier);

        return $this->redirectToRoute('shop_panier');
    }
}
