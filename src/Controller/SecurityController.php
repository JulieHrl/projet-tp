<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\PassType;
use App\Form\InfoUserType;
use App\Form\ResetPassType;
use App\Form\InscriptionType;
use Symfony\Component\Mime\Email;
use App\Repository\UserRepository;
use App\Repository\CommandeRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Bridge\Twig\Mime\TemplatedEmail;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Mailer\MailerInterface;
use App\Repository\CommandeArticlesRepository;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\PasswordType;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
use Symfony\Component\Security\Csrf\TokenGenerator\TokenGeneratorInterface;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;

class SecurityController extends AbstractController
{
    /**
     * @Route("/connexion", name="security_connexion")
     */
    public function connexion()
    {
        return $this->render('security/login.html.twig');
    }

    /**
     * @Route("/inscription", name="security_inscription")
     */

    public function registration(Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder, MailerInterface $mailer)
    {

        if ($this->getUser() && !$this->isGranted("ROLE_ADMIN")) {
            return $this->redirectToRoute('home');
        }

        $user = new User();

        $form = $this->createForm(InscriptionType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //Encodage du password

            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);

            //On génère le token d'activation, aléatoire et unique à l'aide de méthodes natives

            $user->setActivationToken(md5(uniqid()));

            $manager->persist($user);
            $manager->flush();

            //Envoi de l'email contenant le token d'activation

            $email = (new TemplatedEmail())
                ->from('devjulie34@gmail.com')
                ->to($user->getEmail())
                ->subject('Bienvenue à l\'apéro des potes!')
                ->htmlTemplate('emails/activation.html.twig')
                ->context(['token' => $user->getActivationToken()]);

            $mailer->send($email);

            $this->addFlash('success', 'Votre inscription a bien été prise en compte, vous pouvez vous connecter. Pensez à valider votre compte via le lien d\'activation que vous avez reçu par email');

            return $this->redirectToRoute('security_connexion');
        }

        return $this->render('security/inscription.html.twig', [
            'formUser' => $form->createView()
        ]);
    }

    /**
     * @Route("/activation/{token}", name="security_activation")
     */

    public function activation($token, UserRepository $repo, EntityManagerInterface $manager)
    {

        //On vérifie si un utilisateur a ce token
        $user = $repo->findOneBy(['activation_token' => $token]);

        //Si aucun utilisateur n'a ce token
        if (!$user) {
            throw $this->createNotFoundException('Cet utilisateur n\'existe pas');
        }

        //On supprime le token
        $user->setActivationToken(null);
        $manager->persist($user);
        $manager->flush();

        $this->addFlash('success', 'Votre compte a bien été activé!');

        return $this->redirectToRoute('security_connexion');
    }

    /**
     * @Route("/changepass", name="security_forgottenpass")
     */

    public function forgottenpass(Request $request, UserRepository $repo, MailerInterface $mailer, TokenGeneratorInterface $tokengenerator, EntityManagerInterface $manager)
    {

        $form = $this->createForm(ResetPassType::class);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //On récupère les données du formulaire
            $donnees = $form->getData();

            //On cherche dans la base de donnée si un utilisateur correspond à cet email
            $user = $repo->findOneByEmail($donnees['email']);

            //Si l'email ne correspond à aucun utilisateur on renvoie un message d'erreur
            if (!$user) {
                $this->addFlash('danger', 'Cette adresse n\'existe pas');

                return $this->redirectToRoute('security_connexion');
            }

            //On génere un token
            $token = $tokengenerator->generateToken();

            //Qu'on tente d'entrer dans la base de donnée
            try {
                $user->setResetToken($token);

                $manager->persist($user);
                $manager->flush();
            } catch (\Exception $e) {
                $this->addFlash('warning', 'Une erreur est survenue : ' . $e->getMessage());
                return $this->redirectToRoute('security_connexion');
            }

            //On génere l'url de réinitialisation de mot de passe
            $url = $this->generateUrl(
                'security_resetpass',
                [
                    'token' => $token
                ],
                UrlGeneratorInterface::ABSOLUTE_URL
            );

            //On envoie le message
            $email = (new Email())
                ->from('devjulie34@gmail.com')
                ->to($user->getEmail())
                ->subject('Mot de passe oublié')
                ->text("Bonjour, une demande de réinitialisation de mot de passe a été effectuée pour le site L'apéro des potes.
                    Veuillez cliquer sur le lien suivant : " . $url)
                ->html('<p>Bonjour, une demande de réinitialisation de mot de passe a été effectuée pour le site L\'apéro des potes.
            Veuillez cliquer sur le lien suivant : <a href="' . $url . '">Réinitialiser le mot de passe</a></p>');

            $mailer->send($email);

            $this->addFlash('success', 'Un email de réinitialisation du mot de passe vous a été envoyé');

            return $this->redirectToRoute('security_connexion');
        }

        return $this->render('security/forgottenpass.html.twig', [
            'emailForm' => $form->createView()
        ]);
    }

    /**
     * @Route("/resetpass/{token}", name="security_resetpass")
     */

    public function resetpass($token, UserRepository $repo, Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {

        //On cherche l'utilisateur à partir du token

        $user = $repo->findOneBy(['reset_token' => $token]);

        if (!$user) {
            $this->addFlash('danger', 'Token inconnu');
            return $this->redirectToRoute('security_connexion');
        }

        $form = $this->createForm(PassType::class, $user);

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            //On supprime le token
            $user->setResetToken(null);

            //On chiffre le mot de passe
            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', 'Le mot de passe a bien été modifié!');

            return $this->redirectToRoute('home');
        }
        return $this->render('security/resetpass.html.twig', [
            'token' => $token,
            'formPass' => $form->createView()
        ]);
    }



    /**
     * @Route("/deconnexion", name="security_deconnexion")
     */

    public function logout()
    {
    }


    /**
     * @Route("/security/compteuser", name="security_compteuser")
     */

    public function user(UserRepository $userRepo, CommandeRepository $comRepo)
    {

        $user = $this->getUser();

        $commande = $comRepo->findBy(['userId' => $user->getId()]);

        return $this->render('security/compteuser.html.twig', [
            'user' => $user,
            'commande' => $commande
        ]);
    }

    /**
     * @Route("/security/compteuser{id}/infouser", name="security_infouser")
     */

    public function infouser(User $user, Request $request, EntityManagerInterface $manager, UserPasswordEncoderInterface $encoder)
    {

        if ($this->getUser()->getId() != $user->getId() && !$this->isGranted("ROLE_ADMIN")) {
            throw $this->createNotFoundException('Vous n\'avez pas la permission d\'acceder à cette page.');
        }

        $formuser = $this->createForm(InfoUserType::class, $user);

        $formuser->handleRequest($request);

        if ($formuser->isSubmitted() && $formuser->isValid()) {


            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', 'Les modifications ont bien été enregistrées!');


            return $this->redirectToRoute('security_compteuser');
        }

        $formpassword = $this->createFormBuilder($user)
            ->add('password', PasswordType::class)
            ->add('confirm_password', PasswordType::class)
            ->getForm();

        $formpassword->handleRequest($request);

        if ($formpassword->isSubmitted() && $formpassword->isValid()) {

            $hash = $encoder->encodePassword($user, $user->getPassword());

            $user->setPassword($hash);

            $manager->persist($user);
            $manager->flush();

            $this->addFlash('success', 'Le mot de passe a bien été modifié!');

            return $this->redirectToRoute('security_compteuser');
        }


        return $this->render('security/infouser.html.twig', [
            'formuser' => $formuser->createView(),
            'formpassword' => $formpassword->createView()
        ]);

        return $this->render('security/compteuser.html.twig');
    }

    /**
     * @Route("/security/detailcommande/{id}", name="detail_commande")
     * @Security("is_granted('ROLE_USER')")
     */

    public function detailCommande($id, CommandeRepository $comRepo, CommandeArticlesRepository $artRepo)
    {

        $commande = $comRepo->find($id);
        $prodCommande = $artRepo->findAll();

        return $this->render('security/detailcommande.html.twig', [
            'commande' => $commande,
            'produits' => $prodCommande
        ]);
    }
}
