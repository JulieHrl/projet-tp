<?php

namespace App\Controller;

use App\Entity\Avis;
use App\Entity\User;
use App\Repository\AvisRepository;
use App\Repository\UserRepository;
use \App\Entity\Produit;
use App\Repository\CommandeRepository;
use App\Repository\ProduitRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Security;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

/**
 * @Route("/admin")
 * @Security("is_granted('ROLE_ADMIN')")
 */

class AdminController extends AbstractController
{
    /**
     * @Route("/", name="admin_home")
     */
    public function home(AvisRepository $avisRepo, CommandeRepository $comRepo)
    {
        $avis = $avisRepo->findAll();
        $commande = $comRepo->findAll();

        return $this->render('admin/home.html.twig', [
            'avis' => $avis,
            'commande' => $commande
        ]);
    }

    /**
     * @Route("/add", name="admin_add")
     * @Route("/produits{id}/edit", name="admin_edit")
     */
    public function form(Produit $produit = null, Request $request, EntityManagerInterface $manager)
    {
        if (!$produit) {
            $produit = new Produit();
        }

        $form = $this->createFormBuilder($produit)
            ->add('nomproduit')
            ->add('conditionnement')
            ->add('stock')
            ->add('prix')
            ->add('description')
            ->add('compo')
            ->add('image')
            ->add('save', SubmitType::class, [
                'label' => 'Enregistrer'
            ])
            ->getForm();

        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

            $manager->persist($produit);
            $manager->flush();

            $this->addFlash('success', 'Le produit a bien été enregistré');

            return $this->redirectToRoute('admin_edit', ['id' => $produit->getId()]);
        }

        return $this->render('admin/add.html.twig', [
            'formProduit' => $form->createView()
        ]);
    }

    /**
     * @Route("/produits", name="admin_produits")
     */
    public function produits(ProduitRepository $repo)
    {

        $produits = $repo->findAll();

        return $this->render('admin/produits.html.twig', [
            'produits' => $produits
        ]);
    }

    /**
     * @Route("/produits/voiravis/{id}", name="admin_voiravis")
     */
    public function voiravis(Produit $produit, AvisRepository $repo)
    {

        $avis = $produit->getAvisClient();

        return $this->render('admin/voiravis.html.twig', [
            'produit' => $produit,
            'avis' => $avis
        ]);
    }

    /**
     * @Route("/produits/valideavis/{id}", name="admin_valideavis")
     */
    public function valideavis($id, Avis $avis, AvisRepository $repo, EntityManagerInterface $manager)
    {

        $avis = $repo->find($id);

        $idproduit = $avis->getProduit()->getId();

        $avis->setValide(true);

        $manager->persist($avis);
        $manager->flush();

        $this->addFlash('success', 'L\'avis a bien été validé');

        return $this->redirectToRoute('admin_voiravis', ['id' => $idproduit]);
    }

    /**
     * @Route("/produits/deleteavis/{id}", name="admin_deleteavis")
     */
    public function deleteavis($id, Avis $avis, AvisRepository $repo, EntityManagerInterface $manager)
    {

        $avis = $repo->find($id);

        $idproduit = $avis->getProduit()->getId();

        $manager->remove($avis);
        $manager->flush();

        $this->addFlash('success', 'L\'avis a bien été supprimé');

        return $this->redirectToRoute('admin_voiravis', ['id' => $idproduit]);
    }

    /**
     * @Route("/users", name="admin_users")
     */
    public function voirusers(UserRepository $repo)
    {

        $user = $repo->findAll();

        return $this->render('admin/voirusers.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/users/{id}", name="admin_detailuser")
     */
    public function detailuser($id, User $user, UserRepository $repo)
    {

        $user = $repo->find($id);

        return $this->render('admin/detailuser.html.twig', [
            'user' => $user
        ]);
    }

    /**
     * @Route("/users/deleteuser/{id}", name="admin_deleteuser")
     */
    public function deleteuser($id, User $user, UserRepository $repo, EntityManagerInterface $manager)
    {

        $user = $repo->find($id);

        $manager->remove($user);
        $manager->flush();

        $this->addFlash('success', 'L\'utilisateur a bien été supprimé');

        return $this->redirectToRoute('admin_users');
    }
}
