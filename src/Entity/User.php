<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

// Mise en place des contraintes pour les formulaires
use Symfony\Component\Validator\Constraints as Assert;

// Mise en place de la vérification de l'user email afin qu'un email ne soit utilisé qu'une seule fois
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

// Implémentation de l'interface permettant l'encryptage du password
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * @ORM\Entity(repositoryClass="App\Repository\UserRepository")
 * @UniqueEntity(fields={"email"}, message="L'email est déjà utilisé")
 */
class User implements UserInterface
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=3, max=40)
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z-éèàäçï]+$[\'-]?/",
     *     match=true,
     *     message="Le nom n'est pas valide"
     * )
     */
    private $nom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=3, max=40)
     * @Assert\Regex(
     *     pattern="/^[a-zA-Z-éèàäçï]+$[\'-]?/",
     *     match=true,
     *     message="Le prénom n'est pas valide"
     * )
     */
    private $prenom;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Email(
     *      message = "L'email '{{ value }}' n'est pas valide"
     * )
     * @Assert\Regex(
     *     pattern="/^([\w\-\.]+)@([\w\-\.]+)\.([a-zA-Z]{2,5})$/",
     *     match=true,
     *     message="L'email n'est pas valide"
     * )
     */
    private $email;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=8, max=12)
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     match=true,
     *     message="Le téléphone ne peut contenir que des chiffres de 0 à 9"
     * )
     */
    private $telephone;

    /**
     * @ORM\Column(type="text")
     * @Assert\Regex(
     *     pattern="/^[\w\'-. \n]+$/",
     *     match=true,
     *     message="L'adresse n'est pas valide'"
     * )
     */
    private $adresse;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=4, max=8)
     * @Assert\Regex(
     *     pattern="/^\d+$/",
     *     match=true,
     *     message="Le code postal ne peut contenir que des chiffres de 0 à 9"
     * )
     */
    private $codepostal;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=3, max=40)
     * @Assert\Regex(
     *     pattern="/^[a-zA-z\-' ]+$/",
     *     match=true,
     *     message="La commune n'est pas valide"
     * )
     */
    private $commune;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=3, max=40)
     * @Assert\Regex(
     *     pattern="/^[a-zA-z\- ]+$/",
     *     match=true,
     *     message="Le pays n'est pas valide"
     * )
     */
    private $pays;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Length(min=8, minMessage="Votre mot de passe doit faire minimum 8 caractères")
     */
    private $password;

    /**
     * @Assert\EqualTo(propertyPath="password", message="Les mots de passe doivent être identiques")
     */
    public $confirm_password;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Commande", mappedBy="userId")
     */
    private $commandes;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Avis", mappedBy="user")
     */
    private $avis;

    /**
     * @ORM\Column(type="json", nullable=true)
     */
    private $roles = [];

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $activation_token;

    /**
     * @ORM\Column(type="string", length=50, nullable=true)
     */
    private $reset_token;

    public function __construct()
    {
        $this->commandes = new ArrayCollection();
        $this->avis = new ArrayCollection();
    }



    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNom(): ?string
    {
        return $this->nom;
    }

    public function setNom(string $nom): self
    {
        $this->nom = htmlspecialchars($nom);

        return $this;
    }

    public function getPrenom(): ?string
    {
        return $this->prenom;
    }

    public function setPrenom(string $prenom): self
    {
        $this->prenom = htmlspecialchars($prenom);

        return $this;
    }

    public function getEmail(): ?string
    {
        return $this->email;
    }

    public function setEmail(string $email): self
    {
        $this->email = htmlspecialchars($email);

        return $this;
    }

    public function getTelephone(): ?string
    {
        return $this->telephone;
    }

    public function setTelephone(string $telephone): self
    {
        $this->telephone = htmlspecialchars($telephone);

        return $this;
    }

    public function getAdresse(): ?string
    {
        return $this->adresse;
    }

    public function setAdresse(string $adresse): self
    {
        $this->adresse = htmlspecialchars($adresse);

        return $this;
    }

    public function getCodepostal(): ?string
    {
        return $this->codepostal;
    }

    public function setCodepostal(string $codepostal): self
    {
        $this->codepostal = htmlspecialchars($codepostal);

        return $this;
    }

    public function getCommune(): ?string
    {
        return $this->commune;
    }

    public function setCommune(string $commune): self
    {
        $this->commune = htmlspecialchars($commune);

        return $this;
    }

    public function getPays(): ?string
    {
        return $this->pays;
    }

    public function setPays(string $pays): self
    {
        $this->pays = htmlspecialchars($pays);

        return $this;
    }

    public function getPassword(): ?string
    {
        return $this->password;
    }

    public function setPassword(string $password): self
    {
        $this->password = $password;

        return $this;
    }

    /**
     * @return Collection|Commande[]
     */
    public function getCommandes(): Collection
    {
        return $this->commandes;
    }

    public function addCommande(Commande $commande): self
    {
        if (!$this->commandes->contains($commande)) {
            $this->commandes[] = $commande;
            $commande->setUserId($this);
        }

        return $this;
    }

    public function removeCommande(Commande $commande): self
    {
        if ($this->commandes->contains($commande)) {
            $this->commandes->removeElement($commande);
            // set the owning side to null (unless already changed)
            if ($commande->getUserId() === $this) {
                $commande->setUserId(null);
            }
        }

        return $this;
    }

    /**
     * @return Collection|Avis[]
     */
    public function getAvis(): Collection
    {
        return $this->avis;
    }

    public function addAvi(Avis $avi): self
    {
        if (!$this->avis->contains($avi)) {
            $this->avis[] = $avi;
            $avi->setUser($this);
        }

        return $this;
    }

    public function removeAvi(Avis $avi): self
    {
        if ($this->avis->contains($avi)) {
            $this->avis->removeElement($avi);
            // set the owning side to null (unless already changed)
            if ($avi->getUser() === $this) {
                $avi->setUser(null);
            }
        }

        return $this;
    }

    public function getRoles(): ?array
    {
        $roles = $this->roles;

        $roles[] = 'ROLE_USER';

        return array_unique($roles);
    }

    public function setRoles(?array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    public function eraseCredentials()
    {
    }

    public function getSalt()
    {
    }

    public function getUsername()
    {
        return $this->email;
    }

    public function getActivationToken(): ?string
    {
        return $this->activation_token;
    }

    public function setActivationToken(?string $activation_token): self
    {
        $this->activation_token = $activation_token;

        return $this;
    }

    public function getResetToken(): ?string
    {
        return $this->reset_token;
    }

    public function setResetToken(?string $reset_token): self
    {
        $this->reset_token = $reset_token;

        return $this;
    }
}
