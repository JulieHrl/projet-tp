<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

// Mise en place des contraintes pour les formulaires
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="App\Repository\ProduitRepository")
 */
class Produit
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $nomproduit;

    /**
     * @ORM\Column(type="integer", nullable=true)
     */
    private $stock;

    /**
     * @ORM\Column(type="integer")
     */
    private $conditionnement;

    /**
     * @ORM\Column(type="integer")
     */
    private $prix;

    /**
     * @ORM\Column(type="text")
     */
    private $description;

    /**
     * @ORM\Column(type="text")
     */
    private $compo;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\Url()
     */
    private $image;

    /**
     * @ORM\OneToMany(targetEntity="App\Entity\Avis", mappedBy="produit")
     */
    private $avisClient;

    public function __construct()
    {
        $this->avisClient = new ArrayCollection();
    }


    public function getId(): ?int
    {
        return $this->id;
    }

    public function getNomproduit(): ?string
    {
        return $this->nomproduit;
    }

    public function setNomproduit(string $nomproduit): self
    {
        $this->nomproduit = $nomproduit;

        return $this;
    }

    public function getStock(): ?int
    {
        return $this->stock;
    }

    public function setStock(?int $stock): self
    {
        $this->stock = $stock;

        return $this;
    }

    public function getConditionnement(): ?int
    {
        return $this->conditionnement;
    }

    public function setConditionnement(int $conditionnement): self
    {
        $this->conditionnement = $conditionnement;

        return $this;
    }

    public function getPrix(): ?int
    {
        return $this->prix;
    }

    public function setPrix(int $prix): self
    {
        $this->prix = $prix;

        return $this;
    }

    public function getDescription(): ?string
    {
        return $this->description;
    }

    public function setDescription(string $description): self
    {
        $this->description = $description;

        return $this;
    }

    public function getCompo(): ?string
    {
        return $this->compo;
    }

    public function setCompo(string $compo): self
    {
        $this->compo = $compo;

        return $this;
    }

    public function getImage(): ?string
    {
        return $this->image;
    }

    public function setImage(string $image): self
    {
        $this->image = $image;

        return $this;
    }

    /**
     * @return Collection|Avis[]
     */
    public function getAvisClient(): Collection
    {
        return $this->avisClient;
    }

    public function addAvisClient(Avis $avisClient): self
    {
        if (!$this->avisClient->contains($avisClient)) {
            $this->avisClient[] = $avisClient;
            $avisClient->setProduit($this);
        }

        return $this;
    }

    public function removeAvisClient(Avis $avisClient): self
    {
        if ($this->avisClient->contains($avisClient)) {
            $this->avisClient->removeElement($avisClient);
            // set the owning side to null (unless already changed)
            if ($avisClient->getProduit() === $this) {
                $avisClient->setProduit(null);
            }
        }

        return $this;
    }
}
